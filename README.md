# Store Locator Configuration

## Table of contents

- Introduction
- Installation
- Requirements
- Configuration
- Maintainers


## Introduction

This is a very simple module that provides an entity to add store locations.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/store_locator_block).
- Submit bug reports and feature suggestions, or track changes in the
  [issue queue](https://www.drupal.org/project/issues/store_locator_block).


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Requirements

This module requires the following modules:

- [address](https://www.drupal.org/project/address)
- [geolocation](https://www.drupal.org/project/geolocation)


## Configuration

1. Path: admin/config/services/geolocation
   All operations could be done from mentioned path.


## Maintainers

Supporting by:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
